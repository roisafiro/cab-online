import styled from "styled-components";
import { Paper } from "@material-ui/core";

export const SPaper = styled(Paper)`
  padding: 10px;
`;

export const SContainer = styled.div`
  padding: 20px;
  background-color: ${props => props.theme.colors.bg};
`;

export const LoaderWrapper = styled.div`
  height: 100vh;
  width: 100%;
`;

const enumBrandColor = {
  TaxiKurir: "black",
  SverigeTaxi: "yellow"
};

export const PurpelPaper = styled(Paper)`
  padding: 10px;
  background-color: ${props => props.theme.colors.purpel};
  color: ${props => props.theme.typography.colors.white};
`;

export const StyledCabMarker = styled.div`
  width: 25px;
  height: 25px;
  background-color: ${props => props.theme.brandColors[props.brand]};
  border-radius: 50%;
`;

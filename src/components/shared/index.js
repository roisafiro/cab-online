export {
  SPaper as Paper,
  SContainer as Container,
  LoaderWrapper,
  PurpelPaper,
  StyledCabMarker
} from "./components";

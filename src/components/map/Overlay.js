// @flow
import React from "react";
import { OverlayView } from "@react-google-maps/api";
import CabMarker from "./CabMarker";

const MemoisedCab = React.memo(CabMarker);

type Props = {
  coord: {
    lat: number,
    lng: number,
    brand: string
  }
};
const Overlay = ({ coord }: Props) => {
  return (
    <OverlayView
      key={coord.lat}
      mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
      position={coord}
    >
      <MemoisedCab lat={coord.lat} lng={coord.lng} brand={coord.brand} />
    </OverlayView>
  );
};

export default Overlay;

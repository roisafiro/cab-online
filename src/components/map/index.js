// @flow
import React, { useState, useEffect } from "react";
import {
  GoogleMap,
  DirectionsService,
  DirectionsRenderer
} from "@react-google-maps/api";
import socketIOClient from "socket.io-client";
import { useSelector } from "react-redux";
import Overlay from "./Overlay";

const MemoisedOverlay = React.memo(Overlay);

const MapComponent = () => {
  const [coords, setCoords] = useState([]);
  const [response, setResponse] = useState(null);
  const { cab } = useSelector(state => state.map);
  const { destination } = useSelector(state => state.map);
  const { origin } = useSelector(state => state.map);

  useEffect(() => {
    if (destination && destination.lat && origin && origin.lat) {
      const { lat, lng } = origin;
      const endpoint = process.env.REACT_APP_END_POINT
        ? process.env.REACT_APP_END_POINT
        : "http://localhost:4001";
      var socket = socketIOClient(`${endpoint}?lat=${lat}&lng=${lng}`);
      socket.on("getCabsLocationFromApi", data => setCoords(data));
    }
  }, [destination, origin]);

  const directionsCallback = response => {
    if (response !== null) {
      if (response.status === "OK") {
        setResponse(response);
      } else {
        console.log("response: ", response);
      }
    }
  };

  const onLoad = React.useCallback(function onLoad(mapInstance) {
    // const bounds = new window.google.maps.LatLngBounds();
    // mapInstance.fitBounds(bounds);
  });
  return (
    <GoogleMap
      mapContainerStyle={{
        height: "calc(100vh - 60px)",
        width: "100%"
      }}
      zoom={12}
      center={
        origin || {
          lat: 59.32932349999999,
          lng: 18.068580800000063
        }
      }
      onLoad={onLoad}
    >
      {destination && destination.lat && origin && origin.lat && (
        <DirectionsService
          options={{
            destination: destination,
            origin: cab && cab.lat ? cab : origin,
            waypoints:
              cab && cab.lat
                ? [
                    {
                      location: origin,
                      stopover: true
                    }
                  ]
                : null,
            travelMode: "DRIVING"
          }}
          callback={directionsCallback}
          onLoad={directionsService => {
            console.log(
              "DirectionsService onLoad directionsService: ",
              directionsService
            );
          }}
          onUnmount={directionsService => {
            console.log(
              "DirectionsService onUnmount directionsService: ",
              directionsService
            );
          }}
        />
      )}
      {response !== null && (
        <DirectionsRenderer
          options={{
            directions: response
          }}
          onLoad={directionsRenderer => {
            console.log(
              "DirectionsRenderer onLoad directionsRenderer: ",
              directionsRenderer
            );
          }}
          onUnmount={directionsRenderer => {
            console.log(
              "DirectionsRenderer onUnmount directionsRenderer: ",
              directionsRenderer
            );
          }}
        />
      )}
      {coords.map(coord => {
        return <MemoisedOverlay key={coord.lat} coord={coord} />;
      })}
    </GoogleMap>
  );
};

export default MapComponent;

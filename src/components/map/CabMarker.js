// @flow
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Popper,
  Fade,
  Typography,
  CardContent,
  Card,
  CardActions,
  Button,
  CardHeader,
  Avatar
} from "@material-ui/core";
import { mapConstants } from "./../../constants";
import { StyledCabMarker } from "./../shared";

type Props = {
  lat: number,
  lng: number,
  brand: string
};
const CabMarker = ({ lat, lng, brand }: Props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();
  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <React.Fragment>
      <StyledCabMarker
        className="fade-scale-in"
        brand={brand}
        onClick={event => setAnchorEl(anchorEl ? null : event.currentTarget)}
      />
      <Popper
        id={id}
        open={open}
        anchorEl={anchorEl}
        placement="bottom"
        disablePortal={false}
        modifiers={{
          flip: {
            enabled: true
          },
          preventOverflow: {
            enabled: true,
            boundariesElement: "scrollParent"
          },
          arrow: {
            enabled: true
          }
        }}
        transition
      >
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Card>
              <CardContent>
                <CardHeader
                  avatar={<Avatar aria-label="recipe">RR</Avatar>}
                  title="John Smith"
                  subheader={`${brand}, licence: BH23442`}
                />
                <Typography color="textSecondary" gutterBottom>
                  Driver infos
                </Typography>
              </CardContent>
              <CardActions>
                <Button
                  size="small"
                  onClick={() => {
                    dispatch({
                      type: mapConstants.ASSIGN,
                      location: { lat, lng }
                    });
                    setAnchorEl(null);
                  }}
                >
                  Assign
                </Button>
                <Button size="small" variant="contained" color="secondary">
                  Call
                </Button>
              </CardActions>
            </Card>
          </Fade>
        )}
      </Popper>
    </React.Fragment>
  );
};
export default CabMarker;

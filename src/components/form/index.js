// @flow
import React, { useState } from "react";
import { mapConstants } from "./../../constants";
import {
  Grid,
  TextField,
  InputAdornment,
  IconButton,
  RadioGroup,
  InputLabel,
  FormControlLabel,
  Radio,
  MenuItem
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import NumberFormat from "react-number-format";
import { KeyboardTimePicker, KeyboardDatePicker } from "@material-ui/pickers";
import { MyLocation, LocationSearching, AddAlarm } from "@material-ui/icons";
import {
  StandaloneSearchBox,
  DistanceMatrixService
} from "@react-google-maps/api";

const NumberFormatCustom = props => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
            name: "phonenumber"
          }
        });
      }}
      format="+46 (###) ###-####"
      mask="_"
    />
  );
};

type Pos = {
  lat: number,
  lng: number
};

type Props = {
  handleCalculation: Function
};
const Form = ({ handleCalculation }: Props) => {
  const dispatch = useDispatch();
  const { cab } = useSelector(state => state.map);
  const [searchBox, setSearchBox] = useState(null);
  const [locations, setLocations] = useState({
    originLocation: {},
    destinationLocation: {}
  });
  const [values, setValues] = useState({
    origin: "address",
    phonenumber: "",
    destination: "address",
    carclass: "comfort",
    passengers: "1",
    selectedDate: new Date(),
    message: ""
  });
  const [currentLocation, setCurrentLocation]: [
    Pos,
    ((Pos => Pos) | any) => void
  ] = useState({});
  const [originSearchBox, setOriginSearchBox] = useState(null);

  const handleDateChange = date => {
    setValues({
      ...values,
      selectedDate: date
    });
  };

  const handleClickCurrentLocation = () => {
    let pos = null;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          setLocations({ ...locations, originLocation: pos });
          setCurrentLocation(pos);
          dispatch({
            type: mapConstants.ORIGIN,
            location: pos
          });
        },
        () => {
          console.log("something went wrong");
        }
      );
    } else {
      console.log("something went wrong");
    }
  };

  const handleChange = event => {
    let { name, value } = event.target;
    if (name === "carclass") {
      value = Number(values.passengers) > 3 ? "minibus" : value;
    }
    if (name === "passengers") {
      value = Number(value) > 6 ? "6" : Number(value) < 1 ? "1" : value;
    }
    if (name === "carclass") {
      handleCalculation({
        prop: name,
        value: value
      });
    }
    setValues({
      ...values,
      [name]: value
    });
  };

  return (
    <React.Fragment>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
        spacing={1}
      >
        <Grid item xl={6} lg={6} md={12} sm={12} xs={12}>
          <TextField
            select
            fullWidth
            name="origin"
            variant="outlined"
            label="Origin"
            value={values.origin}
            onChange={handleChange}
          >
            <MenuItem value="airport">Airport</MenuItem>
            <MenuItem value="trainstation">Train station</MenuItem>
            <MenuItem value="address">Address</MenuItem>
          </TextField>
          <StandaloneSearchBox
            onLoad={ref => {
              setOriginSearchBox(ref);
            }}
            onPlacesChanged={() => {
              const places = originSearchBox
                ? originSearchBox.getPlaces()
                : null;
              if (places && places[0]) {
                setCurrentLocation({});
                const location = places[0].geometry.location;
                const lat = location.lat();
                const lng = location.lng();
                setLocations({ ...locations, originLocation: { lat, lng } });
                dispatch({
                  type: mapConstants.ORIGIN,
                  location: { lat, lng }
                });
                // handleOrigin({ lat, lng });
              }
            }}
          >
            <TextField
              id="origine-search-box"
              label={
                currentLocation && currentLocation.lat
                  ? "Your Current location"
                  : "Origin"
              }
              fullWidth
              margin="normal"
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      edge="end"
                      aria-label="Current Location"
                      onClick={handleClickCurrentLocation}
                    >
                      {currentLocation && currentLocation.lat ? (
                        <MyLocation color="primary" />
                      ) : (
                        <LocationSearching />
                      )}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </StandaloneSearchBox>
        </Grid>
        <Grid item xl={6} lg={6} md={12} sm={12} xs={12}>
          <TextField
            select
            fullWidth
            name="destination"
            variant="outlined"
            label="Destination"
            value={values.destination}
            onChange={handleChange}
          >
            <MenuItem value="airport">Airport</MenuItem>
            <MenuItem value="trainstation">Train station</MenuItem>
            <MenuItem value="address">Address</MenuItem>
          </TextField>
          <StandaloneSearchBox
            onLoad={ref => {
              setSearchBox(ref);
            }}
            onPlacesChanged={() => {
              const places = searchBox ? searchBox.getPlaces() : null;
              if (places && places[0]) {
                const location = places[0].geometry.location;
                const lat = location.lat();
                const lng = location.lng();
                setLocations({
                  ...locations,
                  destinationLocation: { lat, lng }
                });
                dispatch({
                  type: mapConstants.DESTINATION,
                  location: { lat, lng }
                });
              }
            }}
          >
            <TextField
              id="destination-search-box"
              label="Destination"
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </StandaloneSearchBox>
        </Grid>
        <Grid xl={6} lg={6} md={6} m={12} xs={12} item>
          <KeyboardDatePicker
            margin="normal"
            disablePast
            variant="inline"
            inputVariant="outlined"
            id="date-picker-dialog"
            label="Date picker dialog"
            format="MM/DD/YYYY"
            value={values.selectedDate}
            fullWidth
            onChange={handleDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date"
            }}
          />
        </Grid>
        <Grid xl={6} lg={6} md={6} m={12} xs={12} item>
          <KeyboardTimePicker
            margin="normal"
            id="time-picker"
            variant="inline"
            inputVariant="outlined"
            allowKeyboardControl={false}
            disablePast
            label="Time picker"
            fullWidth
            name="selectedDate"
            value={values.selectedDate}
            onChange={handleDateChange}
            KeyboardButtonProps={{
              "aria-label": "change time"
            }}
            keyboardIcon={<AddAlarm />}
          />
        </Grid>
        <Grid xs={12} item>
          <InputLabel htmlFor="passengers">Passengers</InputLabel>
          <TextField
            id="passengers"
            name="passengers"
            type="number"
            label="How many Passengers"
            fullWidth
            min="1"
            max="6"
            margin="normal"
            variant="outlined"
            value={values.passengers}
            onChange={handleChange}
          />
        </Grid>
        <Grid item>
          <InputLabel htmlFor="carclass">Car class</InputLabel>
          <RadioGroup
            aria-label="Car class"
            name="carclass"
            value={values.carclass}
            onChange={handleChange}
          >
            <FormControlLabel
              value="comfort"
              control={<Radio />}
              disabled={Number(values.passengers) > 3}
              label={
                <>
                  Comfort Class{" "}
                  <small>
                    <i>( 3 seats )</i>
                  </small>
                </>
              }
            />
            <FormControlLabel
              value="business"
              control={<Radio />}
              disabled={Number(values.passengers) > 3}
              label={
                <>
                  Business{" "}
                  <small>
                    <i>( +$23/km, 3 seats )</i>
                  </small>
                </>
              }
            />
            <FormControlLabel
              value="minibus"
              control={<Radio />}
              label={
                <>
                  Minibus{" "}
                  <small>
                    <i>( +$23/km, 6 seats )</i>
                  </small>
                </>
              }
            />
          </RadioGroup>
        </Grid>
        <Grid xs={12} item>
          <TextField
            name="message"
            value={values.message}
            onChange={handleChange}
            fullWidth
            variant="outlined"
            label="Message to driver"
            rows={5}
            rowsMax={10}
          />
        </Grid>
        <Grid xs={12} item>
          <TextField
            name="phonenumber"
            value={values.phonenumber}
            id="formatted-numberformat-input"
            onChange={handleChange}
            label="Phone number"
            fullWidth
            variant="outlined"
            InputProps={{
              inputComponent: NumberFormatCustom
            }}
          />
        </Grid>
        {locations.originLocation &&
          locations.originLocation.lat &&
          locations.destinationLocation &&
          locations.destinationLocation.lat && (
            <DistanceMatrixService
              callback={(response, status) => {
                if (response !== null) {
                  if (status === "OK") {
                    handleCalculation({
                      prop: "journey",
                      value: response.rows[0].elements[0]
                    });
                  } else {
                    console.log("response: ", response);
                  }
                }
              }}
              options={{
                origins:
                  cab && cab.lat
                    ? [locations.originLocation, cab]
                    : [locations.originLocation],
                destinations: [locations.destinationLocation],
                travelMode: "DRIVING"
              }}
              onLoad={() => {
                console.log(
                  "DistanceMatrixService loaded",
                  locations.originLocation
                );
              }}
            />
          )}
      </Grid>
    </React.Fragment>
  );
};

export default Form;

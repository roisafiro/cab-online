import React, { useState } from "react";
import Map from "./components/map";
import NumberFormat from "react-number-format";
import { useLoadScript } from "@react-google-maps/api";
import { CircularProgress, Grid, Typography, Button } from "@material-ui/core";
import {
  Paper,
  PurpelPaper,
  Container,
  LoaderWrapper
} from "./components/shared";
import Form from "./components/form";

const App = () => {
  const [amount, setAmount] = useState(543);
  const [tarif, setTarif] = useState(23);
  const [duration, setDuration] = useState("0mins");
  const [distance, setDistance] = useState("0 km");

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_API_KEY,
    libraries: ["geometry", "drawing", "places"],
    preventGoogleFontsLoading: false
  });

  const reCalculate = value => {
    const minimumAmount = 543;
    let total = minimumAmount;
    if (value && tarif) {
      const valueKm = value.distance.value / 1000;
      total = tarif * valueKm;
      setDistance(value.distance.text);
      setDuration(value.duration.text);
    }
    if (total > minimumAmount) {
      setAmount(total);
    } else {
      setAmount(minimumAmount);
    }
  };
  const onCalculationChange = ({ prop, value }) => {
    if (prop === "carclass") {
      const aditionalCosts = { comfort: 0, business: 12, minibus: 23 };
      const tarif = 23;
      setTarif(tarif + aditionalCosts[value]);
    }
    if (prop === "journey") {
      reCalculate(value);
    } else {
      reCalculate(0);
    }
  };
  if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>;
  }
  return isLoaded ? (
    <Grid
      component={Container}
      container
      direction="row"
      justify="space-around"
      alignItems="flex-start"
      spacing={3}
    >
      <Grid item xl={4} lg={4} md={12} sm={12} xs={12}>
        <Grid container direction="column" spacing={3}>
          <Grid item>
            <Paper>
              <Typography variant="h4">Book your cab</Typography>
              <Typography gutterBottom variant="body2">
                cab-online (v0.1)
              </Typography>
              <Form handleCalculation={onCalculationChange} />
            </Paper>
          </Grid>
          <Grid item>
            <PurpelPaper>
              <Typography gutterBottom variant="h5" component="h2">
                <NumberFormat
                  value={amount}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"kr"}
                />
              </Typography>

              <Typography gutterBottom variant="h6" component="h3">
                ~ {distance} - ~ {duration}
              </Typography>
              <Grid xs={12} item>
                <Button variant="contained" color="secondary" fullWidth>
                  Book
                </Button>
              </Grid>
            </PurpelPaper>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xl={8} lg={8} md={12} sm={12} xs={12}>
        <Paper>
          <Map />
        </Paper>
      </Grid>
    </Grid>
  ) : (
    <Grid
      container
      component={LoaderWrapper}
      direction="row"
      justify="center"
      alignItems="center"
    >
      <Grid item>
        <CircularProgress color="secondary" />
      </Grid>
    </Grid>
  );
};

export default App;

import { mapConstants } from "../constants";

export const map = (state = {}, action) => {
  switch (action.type) {
    case mapConstants.DESTINATION:
      return {
        ...state,
        destination: action.location
      };
    case mapConstants.ORIGIN:
      return {
        ...state,
        origin: action.location
      };
    case mapConstants.ASSIGN:
      return {
        ...state,
        cab: action.location
      };
    default:
      return state;
  }
};

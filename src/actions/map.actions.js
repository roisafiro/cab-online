import { mapConstants } from "../constants";

export const mapActions = {
  destination(location) {
    return { type: mapConstants.DESTINATION, location };
  },
  origin(location) {
    return { type: mapConstants.ORIGIN, location };
  },
  assign(location) {
    return { type: mapConstants.ASSIGN, location };
  }
};

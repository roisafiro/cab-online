export default {
  colors: {
    primary: "linear-gradient(180deg, #1976d2 30%, #116ac1 90%)",
    maincolor: "#dedede",
    purpel: "#3f51b5",
    bg: "#f4f4f4"
  },
  typography: {
    colors: {
      white: "#fff"
    }
  },
  brandColors: {
    TaxiKurir: "black",
    SverigeTaxi: "yellow"
  }
};

const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const axios = require("axios");
const port = process.env.REACT_APP_PORT || 4001;
const index = require("./routes/index");
const app = express();
app.use(index);
const server = http.createServer(app);
const io = socketIo(server);
let interval;
const getApiAndEmit = async socket => {
  try {
    const { lat, lng } = socket.handshake.query;
    const res = await axios.get(
      `https://cabonline-frontend-test.herokuapp.com/vehicles?lat=${lat}&lng=${lng}`
    );
    socket.emit("getCabsLocationFromApi", res.data);
  } catch (error) {
    console.error(`Error: ${error.code}`);
  }
};
io.on("connection", socket => {
  console.log("New client connected");
  getApiAndEmit(socket);
  if (interval) {
    clearInterval(interval);
  }
  interval = setInterval(() => getApiAndEmit(socket), 15000);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});
server.listen(port, () => console.log(`Listening on port ${port}`));
